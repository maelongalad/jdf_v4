var async       = require('async');
var authorize   = require('./authorize');
var parseImages = require('./parse/images');
var parsePages  = require('./parse/pages');
var buildVPTree = require('./vptree');

async.waterfall([
    authorize,
    parsePages,
    parseImages,
    buildVPTree
], function(err) {
    if (err) console.error(err, err.stack);

    process.exit();
});
