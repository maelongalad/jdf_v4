var _        = require('lodash');
var argv     = require('optimist').argv;
var async    = require('async');
var download = require('../../download');
var hasher   = require('../../hasher');
var db       = require('../../DB');
var progress = require('../../util/progress').Progress;
var util     = require('../../util');
var path     = require('path');
var output   = argv.verbose;

module.exports = function(done) {
    if (!argv.i) {
        return done();
    }

    if (output)
        util.out("Processing images\n", "bold");

    async.waterfall([
        getNewImages,
        constructQueue
    ], function(err) {
        if (output)
            util.out("Finished processing images\n", "bold");

        done(err);
    });
};

function getNewImages(done) {
    async.waterfall([
        db.getNewImages,
        function(docs, next) {
            if (docs && docs.length) {
                var arr = _.sortBy(_.pluck(docs, '_id'));
                next(null, arr);
            } else {
                next(null, []);
            }
        }
    ], done);
}

function constructQueue(arr, done) {
    if (output)
        var localProgress = new progress(arr.length);

    var Q = async.queue(handlePage, 10);
    Q.push(arr, function(err) {
        if (err) {
            console.error("\n" + err);
        }

        if (output)
            localProgress.tick();
    });

    Q.drain = function() {
        if (output) {
            localProgress.end();
            util.tick().out("Finished calculating hashes\n", "bold");
        }

        done();
    };
}

function handlePage(id, done) {
    async.waterfall([
        async.apply(download.download, "http://img0.joyreactor.cc/pics/post/full/-" + id + ".png"),
        hasher.getHash,
        function(hash, next) {
            saveImage(id, hash, next);
        }
    ], function(err) {
        if (err) {
            db.errorImage(id, function() {
                done(err);
            });
        } else {
            done();
        }
    });
}

function saveImage(id, hash, done) {
    db.updateImage({_id: id, hash: hash}, done);
}
