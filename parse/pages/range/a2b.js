var _        = require('lodash');
var util     = require('../../../util');
var progress = require('../../../util/progress').Progress;
var async    = require('async');
var argv     = require('optimist').argv;
var output   = argv.verbose;

module.exports = function parseRange(opts) {
    var a     = opts.range[0],
        b     = opts.range[1];
    var range = _.range(a, b + 1, a <= b ? 1 : -1);

    if (output)
        var localProgress = new progress(range.length);

    var Q = async.queue(opts.iterator, 5);
    Q.push(range, function(err) {
        if (err) {
            console.error("\n" + err);
        }

        if (output)
            localProgress.tick();
    });

    Q.drain = function() {
        if (output) {
            localProgress.end();
            util.tick().out("Finished parsing\n", "bold");
        }

        opts.callback();
    };
};
