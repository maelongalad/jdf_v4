var async    = require('async');
var util     = require('../../../util');
var progress = require('../../../util/progress').Progress;
var argv     = require('optimist').argv;
var output   = argv.verbose;
var redis    = require('redis'),
    client   = redis.createClient();

module.exports = function parseNew(opts) {
    var parsedPages   = 0;
    var nextId        = null;
    var newPostsCount = 0;
    var parseTime;

    if (output)
        var localProgress = new progress(-1);

    async.waterfall([
        localIterator,
        function(next) {
            parseTime = Date.now();
            client.set("lastParse", parseTime);
            next();
        },
        function(next) {
            async.whilst(
                test,
                localIterator,
                next
            );
        },
        function(next) {
            client.set("parse:" + parseTime, newPostsCount);
            next();
        }
    ], function(err) {
        if (output) {
            localProgress.end();
            util.tick().out("Finished parsing\n", "bold");
        }

        opts.callback(err);
    });

    function test() {
        return parsedPages <= 3;
    }

    function increment(isNew, count, current, callback) {
        if (isNew) {
            parsedPages = 0;
        } else {
            parsedPages++;
        }

        newPostsCount += count;
        nextId = Number(current) - 1;

        if (output)
            localProgress.tick();

        callback();
    }

    function localIterator(callback) {
        async.waterfall([
            async.apply(opts.iterator, nextId),
            increment
        ], callback);
    }
};
