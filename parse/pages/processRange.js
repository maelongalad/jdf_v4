var async       = require('async');
var requestPage = require('./page/get');
var parsePage   = require('./page/parse');
var saveData    = require('./page/save');

function iterator(jar) {
    return function(id, done) {
        async.waterfall([
            async.apply(requestPage(jar), id !== null ? "/" + id : ""),
            parsePage,
            saveData
        ], done);
    }
}

module.exports = function(jar) {
    return iterator(jar);
};
