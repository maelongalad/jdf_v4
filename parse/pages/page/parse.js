var _       = require('lodash');
var cheerio = require('cheerio');

/**
 * @param {Object} response
 * @param {String} body
 * @param {Function} done
 */
module.exports = function parsePage(response, body, done) {
    var data;
    var $       = cheerio.load(body);
    var posts   = $("div.postContainer");
    var current = $('.pagination_expanded .current').text();

    if (posts.get().length) {
        data = posts.map(extractPostInfo).get();
    } else {
        data = [];
    }

    done(null, data, current);
};

/**
 * @returns {{postID: number, tags, images}}
 */
function extractPostInfo() {
    var images = cheerio('div.post_content div.image img, div.post_content a.video_gif_source', this)
        .map(getURL)
        .get();

    var tags = cheerio('h2.taglist a', this).map(getTag).get();

    images = _.uniq(images, '_id');

    return {
        _id: Number(cheerio(this).attr('id').replace(/[^\d]+/, "")),
        tags: tags,
        images: images
    }
}

/**
 * @returns {{_id: number, hash: String}}
 */
function getURL() {
    var url = "";
    if (this.name == "a") {
        url = cheerio(this).attr('href');
    } else {
        url = cheerio(this).attr('src');
    }

    return {
        _id: Number(url.replace(/.+?([\d]+)\.[\w]+$/, "$1")),
        hash: ""
    };
}

/**
 * @returns {{String}}
 */
function getTag() {
    var tag = cheerio(this);
    var ids = tag.attr('data-ids').split(",");

    return {
        _id: Number(ids[0]),
        name: tag.text(),
        parents: _.map(_.rest(ids), Number)
    };
}
