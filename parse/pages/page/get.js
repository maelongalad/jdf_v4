var request     = require('requestretry');
var lastRequest = Date.now();
var config      = require('../../../config');

/**
 * @param {Object} jar
 * @returns {Function}
 */
module.exports = function(jar) {
    return function requestPage(id, done) {
        var now     = Date.now();
        var delay   = now - lastRequest > config.delay ? 0 : config.delay - now + lastRequest;
        lastRequest = now + delay;

        setTimeout(function() {
            request({
                url: "http://joyreactor.cc/new" + id,
                method: "GET",
                jar: jar,
                maxAttempts: 5,
                retryDelay: 1000
            }, done);
        }, delay);
    }
};
