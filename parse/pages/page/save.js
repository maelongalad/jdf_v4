var _     = require('lodash');
var async = require('async');
var db    = require('../../../DB');

module.exports = function savePosts(posts, current, done) {
    async.mapSeries(posts, iterator, function(err, arr) {
        var newPostsCount = posts.length - _.compact(arr).length;
        done(err, posts.length === newPostsCount, newPostsCount, current);
    });
};

function iterator(post, done) {
    async.parallel([
        async.apply(saveImages, post),
        async.apply(saveTags, post),
        async.apply(savePost, post)
    ], function(err, arr) {
        done(err, arr[2]);
    });
}

function saveImages(post, done) {
    async.map(post.images, db.updateImage, done);
}

function saveTags(post, done) {
    async.map(post.tags, db.updateTag, done);
}

function savePost(post, done) {
    var _post    = {};
    _post._id    = post._id;
    _post.tags   = _.map(post.tags, '_id');
    _post.images = _.map(post.images, '_id');

    db.updatePost(_post, done);
}
