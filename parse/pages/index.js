var argv      = require('optimist').argv;
var async     = require('async');
var processor = require('./processRange');
var util      = require('../../util');
var output    = argv.verbose;

/**
 * @param {Object} jar
 * @param {Function} done
 */
module.exports = function parse(jar, done) {
    if (!(argv.a || argv.b || argv.n)) {
        return done();
    }

    var iterator = processor(jar);

    if (output)
        util.out("Parsing...\n", "bold");

    if (argv.a && argv.b) {
        require('./range/a2b')({
            range: [argv.a, argv.b],
            iterator: iterator,
            callback: done
        });
    } else if (argv.n) {
        require('./range/new')({
            iterator: iterator,
            callback: done
        })
    } else {
        done();
    }
};
