var colors = require('colors/safe');

exports.out = function(str, mode) {
    process.stdout.write("  " + (mode !== undefined ? colors[mode](str) : str) + "\r");
    return module.exports;
};

exports.tick = function() {
    process.stdout.write("✔".green + "\n");
    return module.exports;
};