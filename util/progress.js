var colors = require('colors');
var _      = require('lodash');
var config = require('../config');

function formatHRTime(hrtime) {
    return (hrtime[0] + hrtime[1] / 1e9).toFixed(1);
}

function formatTime(s) {
    if (s <= 60) {
        return s + "s";
    } else if (s > 60 && s <= 3600) {
        return Math.floor(s / 60) + "m" + (s % 60).toFixed(1) + "s";
    } else if (s > 3600) {
        return Math.floor(s / 3600) + "h" + Math.floor((s % 3600) / 60) + "m" + (((s % 3600) / 60) % 60).toFixed(1) + "s";
    }
}

function Progress(length, period) {
    this.total      = length;
    this.period     = period || config.progressTimer;
    this.finite     = length !== -1;
    this.processing = 0;
    this.start      = process.hrtime();
    this.timer      = null;
    this.median     = 0;

    if (process && process.stdout.write) {
        process.stdout.write("  Processed 0" + String(this.finite ? "/" + this.total : "..").yellow + ". Elapsed: 0s;" +
            (this.finite ? " ETA: 0s;" : "") + "  Avg.: 0s.\r");
    }

    this.timer = setInterval(this.output.bind(this), this.period);
}

Progress.prototype = {
    tick: function(info, cb) {
        var diff    = process.hrtime(this.start);
        var elapsed = formatHRTime(diff);
        this.processing++;
        this.median = elapsed / this.processing;
        this.output();
        clearInterval(this.timer);

        if (!this.finite || this.processing < this.total) {
            this.timer = setInterval(this.output.bind(this), this.period);
        }

        if (cb && typeof cb === "function") {
            cb();
        }
    },
    current: function() {
        return this.processing;
    },
    output: function() {
        var diff          = process.hrtime(this.start);
        var progress      = "" + this.processing + (this.finite ? "/" + this.total : "");
        var elapsed       = formatHRTime(diff);
        var elapsedS      = formatTime(elapsed);
        var estimateTotal = this.median !== 0 ? this.median * this.total : elapsed;
        var ETA, ETAs;
        if (this.finite) {
            ETA  = (estimateTotal - elapsed).toFixed(1);
            ETAs = formatTime(ETA);
        }
        var medianS = "" + this.median.toFixed(1) + "s";
        if (process && process.stdout.write && process.stdout.clearLine) {
            process.stdout.clearLine();
            process.stdout.write("  Processed " + String(progress).yellow +
                ". Elapsed: " + String(elapsedS).cyan + (this.finite ? "; ETA: " + String(ETAs).cyan : "") +
                "; Avg.: " + medianS.cyan + ".\r");
        }
    },
    end: function() {
        clearInterval(this.timer);
    }
};

module.exports = {
    formatTime: formatTime,
    formatHRTime: formatHRTime,
    Progress: Progress
};
