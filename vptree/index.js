var _      = require('lodash');
var config = require('../config');
var util   = require('../util');
var async  = require('async');
var argv   = require('optimist').argv;
var db     = require('../DB');
var treeFn = require('./fns');
var path   = require('path');
var VP     = require('./VP');
var redis  = require('redis'),
    client = redis.createClient();
var output = argv.verbose;

module.exports = function calculateVPTree(done) {
    if (!argv.t) {
        return done();
    }

    if (output)
        util.out("Building VP tree\n", "bold");

    async.waterfall([
        db.countHashedImages,
        treeFn.createBlocks,
        function(blocks, count, next) {
            client.set("blocks", JSON.stringify(blocks));
            client.set("hashedImages", count);

            async.eachSeries(blocks, buildBlock, next);
        }
    ], function(err) {
        if (output)
            util.out("Finished building tree\n", "bold");

        done(err);
    });
};
function buildBlock(block, done) {
    var blockName, skip, limit;

    blockName = block.split("..");
    skip      = Number(blockName[0]);
    limit     = blockName[1] === "current" ? _.last(config.split) : Number(blockName[1]) - skip;

    if (output)
        util.out("Block " + block);

    async.waterfall([
        async.apply(checkIfExist, block),
        function(exist, next) {
            if (exist && blockName[1] !== "current") {
                if (output)
                    util.tick();

                done();
            } else {
                next();
            }
        },
        async.apply(db.getHashedImages, skip, limit),
        buildTree,
        function(tree, next) {
            if (output)
                util.tick();

            client.multi()
                .set(block, tree)
                .set(block + ":time", Date.now())
                .save()
                .publish("tree_updated", block)
                .exec(next);
        }
    ], done);
}

function checkIfExist(blockName, done) {
    client.exists(blockName, done);
}

function buildTree(docs, done) {
    var vpTree = VP.build(docs, treeFn.hamming);
    done(null, JSON.stringify(vpTree.tree));
}
