var config = require('../config');

function hashAsString(hash, cb) {
    var i      = 0;
    var bucket = "";
    var string = "";
    if (typeof hash === "string") {
        hash = hash.split("");
    }
    hash.forEach(function(bit) {
        i++;
        bucket += bit;
        if (i == 4) {
            string += parseInt(bucket, 2).toString(16);
            i      = 0;
            bucket = "";
        }
    });
    if (!cb) {
        return string;
    } else if (cb && typeof cb === 'function') {
        cb(null, string);
    }
}

function prependZeros(str, len) {
    if (typeof str === 'number' || !isNaN(Number(str))) {
        str = str.toString();
        return (len - str.length > 0) ? new Array(len + 1 - str.length).join('0') + str : str;
    }
}

function stringAsHash(string, cb) {
    var hash = "";
    for (var i = 0, l = string.length; i < l; i++) {
        hash += prependZeros(parseInt(parseInt(string[i], 16), 10).toString(2), 4);
    }
    if (!cb) {
        return hash;
    } else if (cb && typeof cb === 'function') {
        cb(null, hash);
    }
}

function hamming(a, b) {
    var dist = 0;
    var _a   = a.hash;
    var _b   = b.hash;

    for (var i = 0, l = _a.length; i < l; i++) {
        if (_a[i] != _b[i]) {
            dist++;
        }
    }

    return dist;
}

function createBlocks(result, cb) {
    var i, offset = 0, col = 0, res = result, j;
    var blocks    = [];

    var sizes = config.split;

    for (i = 0; i < sizes.length; i++) {
        col = Math.floor(res / sizes[i]);
        res -= col * sizes[i];

        for (j = 0; j < col;) {
            blocks.push("" + (offset + j * sizes[i]) + ".." + (offset + ++j * sizes[i]))
        }

        offset += col * sizes[i];
    }

    blocks.push("" + offset + "..current");

    if (!cb) {
        return blocks;
    } else if (cb && typeof cb === 'function') {
        cb(null, blocks, result);
    }
}

module.exports = {
    hamming: hamming,
    stringAsHash: stringAsHash,
    hashAsString: hashAsString,
    createBlocks: createBlocks
};
