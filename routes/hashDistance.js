var jwt = require('jsonwebtoken');
var config = require('../config');
var treeFn = require('../vptree/fns');

module.exports = function (req, res) {
    var t1 = jwt.verify(req.query.t1, config.secret);
    var t2 = jwt.verify(req.query.t2, config.secret);
    var h1 = treeFn.stringAsHash(t1.hash);
    var h2 = treeFn.stringAsHash(t2.hash);
    var d = treeFn.hamming({hash: h1}, {hash: h2});
    res.json({distance: d});
};