var async    = require('async'),
    download = require('../download'),
    colors   = require('colors'),
    config   = require('../config');

function uploadFile(req, res, next) {
    var url      = req.body.url && encodeURI(req.body.url),
        file     = req.files.image,
        redirect = req.query.redirect;

    async.waterfall([
        function(next) {
            if (url && file && file.originalName) {
                return next(new Error("Нельзя загрузить два файла одновременно"));
            }

            if (url) {
                return download.download(url, next);
            }

            if (file) {
                if (file.size > config.maxFileSize || file.size === 0) {
                    return next(new Error("Недопустимый размер изображения"));
                }

                if (file.type.split('/')[0] !== 'image') {
                    return next(new Error("Можно загрузить только изображения"));
                }

                return next(null, file.path);
            }

            next(new Error("Файлы не загружены"));
        },
        download.move
    ], function(err, newName) {
        if (err) {
            console.error("URL: %s, file: %s, error: %s".red, url, file && file.originalName, err);
            next(err);
        }

        if (!redirect) {
            res.json({token: String(newName)});
        } else {
            res.status(303);
            res.header('Location', config.api + '/api/hash/' + String(newName) + '?redirect=true');
            res.send("");
        }
    });
}

module.exports = uploadFile;
