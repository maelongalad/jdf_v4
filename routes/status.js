var async  = require('async');
var redis  = require('redis'),
    client = redis.createClient();

module.exports = function(req, res) {
    async.series({
        count: function(next) {
            client.get("hashedImages", next);
        },
        last: function(next) {
            client.get("lastParse", next);
        }
    }, function(err, result) {
        if (err) {
            console.error(String(err));
        }

        res.json({total: result.count, last: result.last});
    });
};
