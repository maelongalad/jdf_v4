var _             = require('lodash'),
    async         = require('async'),
    colors        = require('colors'),
    config        = require('../config'),
    db            = require('../DB'),
    path          = require('path'),
    treeFn        = require('../vptree/fns'),
    VPTreeFactory = require('../vptree/VP'),
    util          = require('../util'),
    redis         = require('redis'),
    client        = redis.createClient(),
    client2       = redis.createClient();
var jwt           = require('jsonwebtoken');

buildTree();

client.subscribe("tree_updated");
client.on("message", function(channel, message) {
    if (channel === "tree_updated") {
        buildTree(message);
    }
});

var lastModified = {};
var cache        = {};
function buildTree(block) {
    async.waterfall([
        function(next) {
            client2.get('blocks', next)
        },
        function(blocks, next) {
            blocks = JSON.parse(blocks);

            var keys = Object.keys(cache);
            for (var i = 0, l = keys.length; i < l; i++) {
                if (blocks.indexOf(keys[i]) === -1) {
                    delete cache[keys[i]];
                    delete lastModified[keys[i]];
                    client2.del(keys[i]);
                }
            }

            if (block) {
                return rebuildBlock(block, next)
            }

            async.eachSeries(blocks, rebuildBlock, next);
        }
    ], function(err) {
        if (err) {
            console.error(String(err).red);
        }

        console.log("all done");
    });
}

function rebuildBlock(block, done) {
    var skip, limit, blockName, blockTime;

    blockName = block.split("..");
    skip      = Number(blockName[0]);
    limit     = blockName[1] === "current" ? _.last(config.split) : Number(blockName[1]) - skip;

    util.out("Block " + block);

    async.waterfall([
        function(next) {
            client2.get(block + ":time", next);
        }, function(time, next) {
            blockTime = time;

            if (lastModified[block] === time) {
                util.tick();
                return done();
            } else {
                next(null);
            }
        },
        async.apply(db.getHashedImages, skip, limit),
        restoreTree(block),
        function(tree, done) {
            cache[block]        = tree;
            lastModified[block] = blockTime;
            util.tick();
            done();
        }
    ], done);
}

function findSimilar(req, res, next) {
    var decoded;

    if (req.params.hash.length == 16) {
        decoded = {
            hash: req.params.hash,
            histogram: '[]'
        }
    } else {
        try {
            decoded = jwt.verify(req.params.hash, config.secret);
        } catch (e) {
            return next(e);
        }
    }

    var hash     = treeFn.stringAsHash(decoded.hash),
        redirect = req.query.redirect;

    async.waterfall([
        function(next) {
            client2.get('blocks', next)
        },
        function(blocks, next) {
            blocks = JSON.parse(blocks);

            async.mapSeries(blocks, function(block, cb) {
                findClosest(cache[block], hash, config.distance, cb);
            }, next);
        },
        function(arr, next) {
            var meta = _.uniq(_.flattenDeep(arr), 'id');

            db.findImageByHash(hash, meta, next);
        },
        function(arr, next) {
            async.mapSeries(arr, db.findPostByImage, next);
        }
    ], function(err, arr) {
        if (err) {
            console.error("Hash: %s, error: %s".red, hash, err);
            return next(err);
        }

        arr = _.compact(_.uniq(_.flattenDeep(arr), 'postId'));

        if (!redirect) {
            res.json({results: arr});
        } else {
            if (arr.length) {
                res.status(303);
                res.header('Location', 'http://joyreactor.cc/post/' + arr[0].postId);
                res.send("");
            } else {
                res.send("No matches");
            }
        }
    });
}

function restoreTree(blockName) {
    return function(docs, done) {
        client2.get(blockName, function(err, tree) {
            tree   = JSON.parse(tree);
            var VP = VPTreeFactory.load(docs, treeFn.hamming, tree);
            done(err, VP);
        });
    }
}

function findClosest(tree, hash, dist, done) {
    var nodes = tree._search({hash: hash}, dist);

    done(null, nodes);
}

module.exports = findSimilar;
