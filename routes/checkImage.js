var request  = require('request');
var config   = require('../config');
var parseuri = require('../download/parseuri');

module.exports = function(req, res) {
    var url    = encodeURI(req.body.url);
    var parsed = parseuri(url);

    request.head(url, function(error, httpResponse) {
        if (error) {
            return res.json({valid: false, error: error});
        }

        if (httpResponse.statusCode !== 200 && httpResponse.statusCode >= 400) {
            return res.json({valid: false, response: 'Ошибка ' + parsed.host + ': код ответа ' + httpResponse.statusCode});
        }

        if (!httpResponse.headers['content-type']) {
            return res.json({valid: false, response: 'Ошибка ' + parsed.host + ': нет заголовка Content-Type'});
        }

        if (httpResponse.headers['content-type'].split('/')[0] != 'image') {
            return res.json({
                valid: false,
                response: 'Невозможно загрузить изображение: не изображение либо неверный Content-Type (' + httpResponse.header['content-type'] + ')'
            });
        }

        if (Number(httpResponse.headers['content-length']) > config.maxFileSize) {
            return res.json({
                valid: false,
                response: 'Невозможно загрузить изображение: размер (' + (Number(httpResponse.headers['content-length']) / 1024 / 1024).toPrecision(2) + 'Мб) превышает максимально допустимый размер (' + (config.maxFileSize / 1024 / 1024).toPrecision(2) + 'Мб)'
            });
        }

        res.json({valid: true});
    });
};
