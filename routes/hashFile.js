var async     = require('async'),
    colors    = require('colors'),
    config    = require('../config'),
    hasher    = require('../hasher'),
    path      = require('path'),
    file      = require('../download');
var jwt       = require('jsonwebtoken');
var stringify = require('../vptree/fns').hashAsString;

function hashFile(req, res, next) {
    var filename = req.params.filename,
        redirect = req.query.redirect;

    hasher.getHash(path.join("/tmp/" + filename), function(err, hash) {
        try {
            hash = stringify(hash);
        } catch (e) {
            console.log(e);
        }
        if (err) {
            console.error("Filename: %s, error: %s".red, filename, err);
            return file.remove(filename, function() {
                next(err);
            });
        }

        if (!redirect) {
            res.json({token: hash});
        } else {
            res.status(303);
            res.header('Location', config.api + '/api/similar/' + hash + '?redirect=true');
            res.send("");
        }
    });
}

module.exports = hashFile;
