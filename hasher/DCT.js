var blockSize = 32;

function dctBasisFunction(l, k, n, m) {
    return Math.cos(((2 * m + 1) * k * Math.PI) / (blockSize * 2)) * Math.cos(((2 * n + 1) * l * Math.PI) / (blockSize * 2));
}

function normFactor(l, k) {
    if (l == 0 && k == 0) {
        return (2 / blockSize) * (1 / 2);
    } else if (l == 0 || k == 0) {
        return (2 / blockSize) * Math.sqrt(1 / 2);
    } else {
        return (2 / blockSize);
    }
}

function getDctCoefficientBlock(pixelBlock) {
    var coefficientBlock = new Array(blockSize * blockSize),
        v, u, x, y;

    for (v = 0; v < blockSize; v++) {
        for (u = 0; u < blockSize; u++) {

            coefficientBlock[v * blockSize + u] = 0;

            for (y = 0; y < blockSize; y++) {
                for (x = 0; x < blockSize; x++) {
                    coefficientBlock[v * blockSize + u] += pixelBlock[y * blockSize + x] * dctBasisFunction(v, u, y, x);
                }
            }

            coefficientBlock[v * blockSize + u] *= normFactor(v, u);
        }
    }

    return coefficientBlock;
}

function getHash(block, done) {
    var coefficientBlock,
        grayBlock = [],
        hash      = [],
        x, y, sum = 0, avg;

    for (var i = 0, len = block.length; i < len; i += 4) {
        grayBlock.push(block[i]);
    }

    coefficientBlock = getDctCoefficientBlock(grayBlock);
    for (y = 1; y < blockSize; y++) {
        for (x = 1; x < blockSize; x++) {
            if (x < 9 && y < 9) {
                sum += coefficientBlock[x + y * blockSize];
            }
        }
    }

    avg = sum / 64;

    for (y = 1; y < blockSize; y++) {
        for (x = 1; x < blockSize; x++) {
            if (x < 9 && y < 9) {
                if (coefficientBlock[x + y * blockSize] >= avg) {
                    hash.push(1);
                } else {
                    hash.push(0);
                }
            }
        }
    }

    done(null, hash.join(""));
}

module.exports = {
    getHash: getHash
};
