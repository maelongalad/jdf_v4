var async  = require('async'),
    canvas = require('canvas'),
    DCT    = require('./DCT');

/**
 * @param {Buffer} img
 * @param {Function} done
 */
function calcHash(img, done) {
    async.waterfall([
        async.apply(prepare, img),
        DCT.getHash
    ], done);
}

/**
 * @param {Buffer} img
 * @param {Function} done
 */
function prepare(img, done) {
    var w         = img.width, h = img.height,
        cv        = new canvas(w, h),
        ctx       = cv.getContext('2d'),
        imgData;

    ctx.save();
    ctx.fillStyle = 'rgb(255, 255, 255)';
    ctx.fillRect(0, 0, w, h);
    ctx.restore();
    ctx.drawImage(img, 0, 0);
    imgData       = ctx.getImageData(0, 0, w, h).data;

    done(null, imgData);
}

module.exports = {
    calcHash: calcHash
};
