var async = require('async'),
    cv = require('./canvas'),
    fs = require('fs'),
    gm = require('gm'),
    Image = require('canvas').Image,
    path = require('path');

/**
 * @param {String} filename
 * @param done
 */
function getHash(filename, done) {
    async.waterfall([
        async.apply(gmImage, filename),
        function (thumbnail, next) {
            async.waterfall([
                function (next) {
                    var img = new Image;

                    img.onload = function () {
                        next(null, img);
                    };
                    img.onerror = next;
                    img.src = thumbnail;
                },
                cv.calcHash
            ], next);
        },
        function (hash, next) {
            fs.unlink(filename, function (err) {
                next(err, hash);
            });
        }
    ], done);
}

function gmImage(filename, done) {
    async.waterfall([
        function (next) {
            gm(filename + "[0]")
                .noProfile()
                .options({imageMagick: true})
                .write("PNG:" + filename, next);
        },
        function (a, b, c, next) {
            resizeAndRead(next);
        }
    ], done);

    function resizeAndRead(next) {
        async.waterfall([
            function (next) {
                gm(filename)
                    //.shave(5, 5)
                    //.fuzz(5, true)
                    //.trim()
                    //.repage("+")
                    .convolve(
                        1, 1, 1, 1, 1, 1, 1,
                        1, 1, 1, 1, 1, 1, 1,
                        1, 1, 1, 1, 1, 1, 1,
                        1, 1, 1, 1, 1, 1, 1,
                        1, 1, 1, 1, 1, 1, 1,
                        1, 1, 1, 1, 1, 1, 1,
                        1, 1, 1, 1, 1, 1, 1
                    )
                    .filter("Catrom")
                    .resize(32, 32, "!")
                    .modulate(100, 0)
                    .write(filename, next);
            },
            function (a, b, c, next) {
                fs.readFile(filename, next);
            }
        ], next);
    }
}

module.exports = {
    getHash: getHash
};
