var Image       = require('./DB/models').Image;
var async       = require('async');
var exec        = require('child_process').exec;
var path        = require('path');
var progress    = require('./util/progress').Progress;
var util        = require('./util');
var argv        = require('optimist').argv;
var output      = argv.verbose;
var concurrency = argv.n || 12;

if (output)
    util.out("Processing images\n", "bold");
async.forever(
    function(next) {
        async.waterfall([
            function(next) {
                Image.count({__v: 0}, next);
            },
            function(count, next) {
                util.out(count + "\n");
                //Image.find({tries: {$exists: false}}, {_id: 1, __v: 1, parsed: 1}, {lean:
                // true}).limit(10000).exec(next);
                Image.find({__v: 0}, {_id: 1}, {lean: true}).limit(10000).exec(next);
            },
            function(docs, next) {
                if (output)
                    var localProgress = new progress(docs.length);

                var Q   = async.queue(execTask, concurrency);
                Q.push(docs, function(err) {
                    if (err) {
                        console.error("\n" + err);
                    }

                    if (output)
                        localProgress.tick();
                });
                Q.drain = function() {
                    if (output) {
                        localProgress.end();
                        util.tick().out("Finished calculating hashes\n", "bold");
                    }

                    next();
                };
            }
        ], next);
    },
    function(err) {
        console.error(err);

        process.exit();
    }
);

function execTask(task, done) {
    //Image.findByIdAndUpdate(
    //    task._id,
    //    {
    //        $set: {
    //            __v: 0,
    //            tries: task.__v,
    //            parsed: task.parsed === 1
    //        }
    //    },
    //    done
    //);

    exec('node ' + path.join(__dirname, 'update.js') + ' -i ' + String(task._id), function(err, stdout, stderr) {
        done(err);
    });
}
