var async     = require('async');
var cheerio   = require('cheerio');
var config    = require('../config');
var request   = require('request');
var cookieJar = request.jar();
var util      = require('../util');
var argv      = require('optimist').argv;
var output    = argv.verbose;

/**
 * @param {Function} done
 */
module.exports = function authorize(done) {
    if (!(argv.b || argv.n || argv.a)) {
        return done(null, undefined);
    }

    if (output)
        util.out("Authorizing...\n", "bold");

    async.waterfall([
        getLoginPage,
        parseLoginPage,
        makeLogin
    ], function(err) {
        if (output)
            util.tick().out("Authorization successful\n", "bold");

        done(err, cookieJar);
    });
};

/**
 * @param {Function} done
 */
function getLoginPage(done) {
    if (output)
        util.out("Getting login page");

    request({
        url: "http://joyreactor.cc/login",
        method: "GET",
        jar: cookieJar
    }, done);
}

/**
 * @param {Object} response
 * @param {String} body
 * @param {Function} done
 */
function parseLoginPage(response, body, done) {
    if (output)
        util.tick().out("Parsing login page");

    var $          = cheerio.load(body);
    var csrf_token = $('#signin__csrf_token').val();
    done(null, csrf_token);
}

/**
 * @param {String} csrf_token
 * @param {Function} done
 */
function makeLogin(csrf_token, done) {
    if (output)
        util.tick().out("Attempting login");

    request({
        url: "http://joyreactor.cc/login",
        method: "POST",
        jar: cookieJar,
        form: {
            "signin[username]": config.user.name,
            "signin[password]": config.user.password,
            "signin[remember]": "on",
            "signin[_csrf_token]": csrf_token
        }
    }, done)
}
