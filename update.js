var Image    = require('./DB/models').Image;
var db       = require('./DB/index');
var async    = require('async');
var download = require('./download');
var hasher   = require('./hasher');
var redis    = require('redis'),
    client   = redis.createClient();

function updateImage(id, next) {
    async.waterfall([
        function(next) {
            async.parallel({
                doc: function(next) {
                    Image.findById(id, {__v: 1, _id: 0}, {lean: true}, next);
                },
                redis: function(next) {
                    client.exists("parsing:" + id, next);
                }
            }, next);
        },
        function(res, next) {
            if (res.doc.__v === 1 || res.redis) {
                return next(new Error("Already parsing"));
            }

            async.parallel({
                redis: function(next) {
                    client.multi()
                        .set("parsing:" + id, true)
                        .expire("parsing:" + id, 60)
                        .exec(next);
                },
                download: function(next) {
                    download.download("http://img0.joyreactor.cc/pics/post/full/-" + id + ".jpg", next);
                }
            }, next);
        },
        function(res, next) {
            next(null, res.download);
        },
        hasher.getHash,
        function(hash, histogram, next) {
            async.series([
                function(next) {
                    Image.findByIdAndUpdate(
                        id,
                        {
                            $set: {
                                hash: hash,
                                histogram: histogram,
                                parsed: true,
                                __v: 1
                            }
                        },
                        next
                    );
                },
                function(next) {
                    client.del("parsing:" + id, next);
                }
            ], next);
        }
    ], function(error) {
        if (error) {
            return db.errorImage(id, next);
        }
        next(error);
    });
}

var id = require('optimist').argv.i;

updateImage(id, function(err) {
    if (err) {
        console.log(err);
    }
    process.exit();
});
