var fs    = require('fs'),
    async = require('async');

async.waterfall([
    function(next) {
        fs.readdir("/tmp", next);
    },
    function(nodes, next) {
        async.each(nodes, deleteOld, next);
    }
], function() {
    process.exit();
});

function deleteOld(node, next) {
    async.waterfall([
        function(next) {
            fs.stat("/tmp/" + node, next);
        },
        function(stat, next) {
            if (stat.isFile() && isOld(stat)) {
                fs.unlink("/tmp/" + node, next);
            }
        }
    ], next);
}

function isOld(stat) {
    var time = new Date(stat.ctime);

    return Date.now() - time.getTime() > 600;
}
