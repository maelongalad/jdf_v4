var crypto   = require('crypto');
var fs       = require('fs');
var config   = require('../config');
var path     = require('path');
var parseuri = require('./parseuri');
var request  = require('request');
var _2ch     = require('./2ch');

/**
 * @param {String} url
 * @param {Function} done
 */
exports.download = function(url, done) {
    var filename = '/tmp/' + name(url.substring(url.lastIndexOf('/')) + 1),
        file     = fs.createWriteStream(filename),
        valid    = true,
        parsed   = parseuri(url),
        ref      = parsed.protocol + "://" + parsed.host + "/",
        options  = {
            url: url,
            timeout: 45000,
            headers: {
                Referer: ref,
                "User-Agent": config.userAgent
            },
            maxAttempts: 3,
            retryDelay: 1000
        };

    file
        .on('error', function(err) {
            remove(filename, '', function() {
                done(err, filename);
            });
        })
        .on('finish', function() {
            if (!valid) {
                done(null, filename);
            } else {
                remove(filename, '', function() {
                    done(new Error(valid), filename);
                });
            }
        });

    if (config.proxyHosts.indexOf(parsed.host) !== -1) {
        _2ch(url, onResponse, onError, file);
    } else {
        request(options)
            .on('response', onResponse)
            .on('error', onError)
            .pipe(file);
    }

    function onResponse(resp) {
        if ((valid = responseIsValid(resp, parsed.host))) {
            resp.socket.end();
        }
    }

    function onError(err) {
        remove(filename, '', function() {
            done(err, filename);
        });
    }
};

/**
 * @param {String} filename
 * @returns {String}
 */
function name(filename) {
    return crypto.createHash('md5').update(filename + String(Date.now()) + config.salt).digest('hex');
}

/**
 * @param {Object} response
 * @returns {boolean}
 */
function responseIsValid(response, host) {
    if (response.statusCode >= 300 && response.statusCode < 400)
        return false;

    if (response.statusCode !== 200) {
        return 'Ошибка ' + host + ': код ответа ' + response.statusCode;
    }

    if (!response.headers['content-type']) {
        return 'Ошибка ' + host + ': нет заголовка Content-Type';
    }

    if (response.headers['content-type'].split('/')[0] != 'image') {
        return 'Невозможно загрузить изображение: не изображение либо неверный Content-Type (' +
            response.headers['content-type'] + ')';
    }

    if (Number(response.headers['content-length']) > config.maxFileSize) {
        return 'Невозможно загрузить изображение: размер (' +
            (Number(response.headers['content-length']) / 1024 / 1024).toPrecision(2) +
            'Мб) превышает максимально допустимый размер (' + (config.maxFileSize / 1024 / 1024).toPrecision(2) + 'Мб)';
    }

    return false;
}

/**
 * @param {String} filename
 * @param {Function} done
 */
exports.move = function(filename, done) {
    var genericName = name(filename);
    fs.rename(filename, path.join('/tmp/', genericName), function(err) {
        done(err, genericName);
    });
};

/**
 * @param {String} filename
 * @param {String} hash
 * @param {Function} done
 */
function remove(filename, hash, done) {
    try {
        fs.unlink(filename, function(err) {
            done(err, filename, hash);
        });
    } catch (e) {
        console.error("\n", e);
        done(null, filename, hash);
    }
}

exports.remove = remove;
