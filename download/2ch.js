var config    = require('../config');
var request   = require('request');
var cookieJar = request.jar();

module.exports = function(url, onResponse, onError, file) {
    request({
        url: "https://www.proxfree.com/proxy/",
        method: "GET",
        jar: cookieJar
    }, function() {
        request({
            url: "https://de.proxfree.com/request.php?do=go",
            method: "POST",
            form: {
                "get": url,
                pfserverDropdown: "https://de.proxfree.com/request.php?do=go",
                allowCookies: "on",
                pfipDropdown: "default"
            },
            jar: cookieJar
        })
            .on('response', function(resp) {
                var redirect = resp.headers.location;

                request({
                    url: redirect,
                    method: "GET",
                    jar: cookieJar
                })
                    .on('response', onResponse)
                    .on('error', onError)
                    .pipe(file);
            })
            .on('error', onError);
    }).on('error', onError);
};
