var mongoose = require('mongoose');
var config   = require('../config');

mongoose.connect("mongodb://" + config.db.user + ":" + config.db.password + "@" + config.db.name);

exports.Post = mongoose.model(
    'Post',
    mongoose.Schema({
        _id: {type: Number, index: {unique: true}},
        tags: [Number],
        images: [{
            type: Number, index: true
        }]
    })
);

exports.Image = mongoose.model(
    'Image',
    mongoose.Schema({
        _id: {type: Number, index: {unique: true}},
        hash: {type: String, index: true},
        parsed: {type: Boolean, index: true},
        tries: {type: Number, default: 0},
        error: {type: Boolean, default: false},
        histogram: [String]
    })
);

exports.Tag = mongoose.model(
    'Tag',
    mongoose.Schema({
        _id: {type: Number, index: {unique: true}},
        parents: [Number],
        name: String
    })
);
