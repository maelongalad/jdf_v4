var models = require('./models');
var async  = require('async');
var _      = require('lodash');

exports.updateImage = function(obj, done) {
    obj.parsed = obj.hash !== "";
    obj.__v    = 1;

    models.Image.findById(obj._id, function(err, doc) {
        if (doc && doc.hash === "") {
            doc.update({$set: {hash: obj.hash, parsed: obj.parsed, histogram: obj.histogram, __v: obj.__v}}, done);
        } else if (!doc) {
            models.Image.create(obj, done);
        } else if (doc && doc.hash !== "") {
            done(null, doc);
        }
    });
};

exports.getHistogram = function(id, done) {
    models.Image.findById(id, {_id: 0, histogram: 1}, {lean: true}, done);
};

exports.errorImage = function(id, done) {
    models.Image.findById(id, function(err, doc) {
        if (doc.tries >= 3) {
            doc.update({$set: {error: true}}, done);
        } else {
            doc.update({$inc: {tries: 1}}, done);
        }
    });
};

exports.updateTag = function(obj, done) {
    models.Tag.findByIdAndUpdate(obj._id, {$set: {parents: obj.parents, name: obj.name}}, {
        upsert: true,
        lean: true
    }, done);
};

exports.updatePost = function(obj, done) {
    models.Post.findByIdAndUpdate(obj._id, {$set: {tags: obj.tags, images: obj.images}}, {
        upsert: true,
        lean: true
    }, done);
};

exports.getNewImages = function(done) {
    models.Image.find({parsed: false, error: false}, {_id: 1}, {lean: true}, done);
};

exports.countHashedImages = function(done) {
    models.Image.count({parsed: true}, done);
};

exports.getHashedImages = function(skip, limit, done) {
    models.Image.find({parsed: true}, {_id: 1, hash: 1}, {lean: true})
        .sort({_id: 1})
        .skip(skip)
        .limit(limit)
        .exec(done);
};

exports.findPostByImage = function(obj, done) {
    models.Post.findOne({images: obj.id}, {_id: 1}, {lean: true}, function(err, post) {
        if (err) {
            return done(err);
        }

        if (post == null) {
            return done(null, null);
        }

        done(null, {
            postId: post._id,
            imageId: obj.id,
            distance: obj.d
        });
    });
};

exports.findImageByHash = function(hash, arr, done) {
    models.Image.find({hash: hash}, {_id: 1}, {lean: true}, function(err, images) {
        if (err) {
            return done(err);
        }

        done(null, _.union(
            arr,
            _.map(images, function(image) {
                return {
                    id: image.id,
                    d: 0
                }
            })
        ))
    });
};
