var restify = require('restify'),
    server  = restify.createServer({
        name: "bayanometr API",
        formatters: {
            'application/json': function(req, res, body) {
                if (body instanceof Error) {
                    res.statusCode = body.statusCode || 500;

                    body = {
                        error: body.body && body.body.message || body.message
                    };
                } else if (Buffer.isBuffer(body)) {
                    body = body.toString('base64');
                }

                var data = JSON.stringify(body);
                res.setHeader('Content-Length', Buffer.byteLength(data));

                return data;
            }
        }
    }),
    config  = require('./config');

server.use(restify.bodyParser({limit: config.maxFileSize}));
server.use(restify.queryParser());

server.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});
server.post('/api/v1/upload', require('./routes/uploadFile'));
server.post('/api/v1/report', require('./routes/reportForm'));
server.get('/api/v1/hash/:filename', require('./routes/hashFile'));
server.get('/api/v1/similar/:hash', require('./routes/findSimilar'));
server.get('/api/v1/status', require('./routes/status'));
server.post('/api/v1/check', require('./routes/checkImage'));
server.get('/api/v1/distance', require('./routes/hashDistance'));

server.on('NotFound', function(req, res, err, next) {
    res.status(err.status || err.statusCode || 500);
    res.json({error: err.message});
    next();
});
server.on('MethodNotAllowed', function(req, res, err, next) {
    res.status(err.status || err.statusCode || 500);
    res.json({error: err.message});
    next();
});
server.on('unhandledException', function(req, res, route, err) {
    res.status(err.status || err.statusCode || 500);
    res.json({error: err.message});
});

server.listen(config.port, function() {
    console.log('%s listening at %s', server.name, server.url);
});
